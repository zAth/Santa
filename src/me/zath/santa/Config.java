package me.zath.santa;
/*
 * MC
 * Created by zAth
 */

public class Config {

    private String msg_NoPermisssion, msg_AlreadySanta, msg_NewSanta, msg_Location;

    public Config() {
        msg_NoPermisssion = getString("Msg.NoPermission").replaceAll("&", "§");
        msg_AlreadySanta = getString("Msg.AlreadySanta").replaceAll("&", "§");
        msg_NewSanta = getString("Msg.NewSanta").replaceAll("&", "§");
        msg_Location = getString("Msg.Location").replaceAll("&", "§");
    }

    public String getMsg_Location() {
        return msg_Location;
    }

    public String getMsg_NewSanta() {
        return msg_NewSanta;
    }

    public String getMsg_AlreadySanta() {
        return msg_AlreadySanta;
    }

    public String getMsg_NoPermisssion() {
        return msg_NoPermisssion;
    }

    public String getString(String path) {
        String toReturn;
        try {
            toReturn = Santa.getSanta().getConfig().getString(path);
        } catch (Exception e) {
            Santa.getSanta().getServer().getPluginManager().disablePlugin(Santa.getSanta());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }
}
