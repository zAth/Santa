package me.zath.santa.listeners;
/*
 * MC 
 * Created by zAth
 */

import me.zath.santa.Santa;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

public class DropListener implements Listener {

    @EventHandler
    public void onChestSpawn(EntityChangeBlockEvent e) {
        if (Santa.getSanta().getDropManager().isDrop(e.getEntity())) {
            e.setCancelled(true);
            Santa.getSanta().getDropManager().solidify(e.getEntity());
        }
    }

    @EventHandler
    public void onChestClick(PlayerInteractEvent event){
        if(event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        if(!event.hasBlock() || event.getClickedBlock() == null) return;
        if(!Santa.getSanta().getDropManager().isChest(event.getClickedBlock())) return;

        Inventory inventory = Santa.getSanta().getServer().createInventory(null, 9 * 3);
        inventory.setItem(13, Santa.getSanta().getDropManager().getPrize(event.getClickedBlock()));
        event.getPlayer().openInventory(inventory);

        Santa.getSanta().getDropManager().sublimate(event.getClickedBlock());
    }

}
