package me.zath.santa.listeners;
/*
 * MC 
 * Created by zAth
 */

import me.zath.santa.Santa;
import me.zath.santa.objects.Sleigh;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;

public class SleighListener implements Listener {

    @EventHandler
    public void onVehicleExit(VehicleExitEvent event) {
        if (event.getVehicle().getType() != EntityType.BOAT) return;
        if (event.getExited().getType() != EntityType.PLAYER) return;
        Player player = (Player) event.getExited();

        if (Santa.getSanta().getSantaManager().isMasked(player))
            Santa.getSanta().getSantaManager().unmask(player);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (Santa.getSanta().getSantaManager().isMasked(event.getPlayer()))
            Santa.getSanta().getSantaManager().unmask(event.getPlayer());
    }

    @EventHandler
    public void onChestClick(PlayerInteractEvent event) {
        if (!Santa.getSanta().getSantaManager().isMasked(event.getPlayer())) return;
        Sleigh sleigh = Santa.getSanta().getSantaManager().getSleigh(event.getPlayer());

        if (event.getAction() == Action.RIGHT_CLICK_AIR)
            sleigh.drop();
        else if (event.getAction() == Action.LEFT_CLICK_AIR)
            sleigh.setStopped(!sleigh.isStopped());
    }

    @EventHandler
    public void onVehicleDestroy(VehicleDestroyEvent event) {
        if (event.getAttacker() != null) return;
        if (event.getVehicle().getType() != EntityType.BOAT) return;
        if (event.getVehicle().getPassenger() == null || event.getVehicle().getPassenger().getType() != EntityType.PLAYER)
            return;
        if (!Santa.getSanta().getSantaManager().isMasked((Player) event.getVehicle().getPassenger())) return;

        event.setCancelled(true);
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getEntity().getType() == EntityType.PLAYER) {
            if (Santa.getSanta().getSantaManager().isMasked((Player) event.getEntity()))
                event.setCancelled(true);
        } else if (event.getEntity().getType() == EntityType.HORSE) {
            if (Santa.getSanta().getSantaManager().isReindeer((Horse) event.getEntity()))
                event.setCancelled(true);
        }
    }

}
