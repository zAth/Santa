package me.zath.santa.managers;
/*
 * MC 
 * Created by zAth
 */

import me.zath.santa.objects.Runnable;
import me.zath.santa.objects.Sleigh;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class SantaManager {

    private HashMap<Player, Sleigh> santas;
    private Runnable runnable;

    public SantaManager() {
        santas = new HashMap<>();
        runnable = new Runnable();
    }

    public void mask(Player player){
        if(player.getVehicle() != null)
            player.getVehicle().eject();

        Sleigh sleigh = new Sleigh(player);
        santas.put(player, sleigh);
    }

    public void unmask(Player player){
        santas.get(player).remove();
        santas.remove(player);
    }

    public boolean isMasked(Player player){
        return santas.containsKey(player);
    }

    public Sleigh getSleigh(Player player){
        return santas.get(player);
    }

    public HashMap<Player, Sleigh> getSantas() {
        return santas;
    }

    public Runnable getRunnable() {
        return runnable;
    }

    public boolean isReindeer(Horse entity) {
        return santas.values().stream().filter(sleigh -> sleigh.isReindeer(entity)).findAny().isPresent();
    }
}
