package me.zath.santa.managers;
/*
 * MC 
 * Created by zAth
 */

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class DropManager {

    private HashMap<FallingBlock, ItemStack> drops;
    private HashMap<Block, ItemStack> chests;

    public DropManager() {
        drops = new HashMap<>();
        chests = new HashMap<>();
    }

    public void drop(ItemStack _itemStack, Location location){
        ItemStack itemStack = new ItemStack(Material.COAL);
        if(_itemStack != null && _itemStack.getType() != Material.AIR)
            itemStack = _itemStack;

        FallingBlock fallingBlock = location.getWorld().spawnFallingBlock(location, 41, (byte) 0);
        fallingBlock.setHurtEntities(false);
        fallingBlock.setDropItem(false);

        drops.put(fallingBlock, itemStack);
    }

    public boolean isDrop(Entity entity) {
        return entity.getType() == EntityType.FALLING_BLOCK && drops.containsKey(entity);
    }

    public boolean isChest(Block block){
        return block.getType() == Material.CHEST && chests.containsKey(block);
    }

    public void sublimate(Block block){
        block.setType(Material.AIR);
        chests.remove(block);

        block.getWorld().playSound(block.getLocation(), Sound.LEVEL_UP, 1, 1);
    }

    public void solidify(Entity entity){
        Block block = entity.getLocation().getBlock();
        block.setType(Material.CHEST);

        chests.put(block, drops.get(entity));

        drops.remove(entity);
        entity.remove();

        block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, 1, 1);
    }

    public ItemStack getPrize(Block block){
        return chests.get(block);
    }

    public void clear(){
        drops.keySet().forEach(Entity::remove);
        chests.keySet().forEach(block -> block.setType(Material.AIR));
    }

    public HashMap<Block, ItemStack> getChests() {
        return chests;
    }
}
