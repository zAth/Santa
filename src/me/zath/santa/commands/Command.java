package me.zath.santa.commands;
/*
 * MC 
 * Created by zAth
 */

import me.zath.santa.Santa;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] args) {
        if(!command.getName().equalsIgnoreCase("santa")) return true;
        if(!(commandSender instanceof Player)){
            commandSender.sendMessage("§4Comando apenas para jogadores, de verdade!");
            return true;
        }
        Player player = (Player) commandSender;

        if(!player.hasPermission("santa.use")){
            player.sendMessage(Santa.getSanta().getConfiguration().getMsg_NoPermisssion());
            return true;
        }

        if(args.length != 0){
            Santa.getSanta().getDropManager().getChests().keySet().forEach(block ->
                player.sendMessage(Santa.getSanta().getConfiguration().getMsg_Location()
                    .replaceAll("%world%", block.getWorld().getName())
                    .replaceAll("%x%", "" + block.getLocation().getBlockX())
                    .replaceAll("%y%", "" + block.getLocation().getBlockY())
                    .replaceAll("%z%", "" + block.getLocation().getBlockZ())));
            return true;
        }

        if(Santa.getSanta().getSantaManager().isMasked(player)){
            player.sendMessage(Santa.getSanta().getConfiguration().getMsg_AlreadySanta());
            return true;
        }

        Santa.getSanta().getSantaManager().mask(player);
        Santa.getSanta().getServer().getOnlinePlayers().forEach(onlinePlayer ->
            onlinePlayer.sendMessage(Santa.getSanta().getConfiguration().getMsg_NewSanta()
                .replaceAll("%world%", player.getWorld().getName())
                .replaceAll("%x%", "" + player.getLocation().getBlockX())
                .replaceAll("%y%", "" + player.getLocation().getBlockY())
                .replaceAll("%z%", "" + player.getLocation().getBlockZ())
            )
        );

        return false;
    }

}
