package me.zath.santa.objects;
/*
 * MC 
 * Created by zAth
 */

import me.zath.santa.Santa;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Iterator;

public class Runnable extends BukkitRunnable {

    public Runnable() {
        this.runTaskTimer(Santa.getSanta(), 0, 5);
    }

    @Override
    public void run() {
        Iterator<Player> santasIterator = Santa.getSanta().getSantaManager().getSantas().keySet().iterator();
        while(santasIterator.hasNext()){
            Player nextSanta = santasIterator.next();
            Sleigh sleigh = Santa.getSanta().getSantaManager().getSleigh(nextSanta);

            boolean toRemove = sleigh.tick();

            if(toRemove){
                sleigh.remove();
                santasIterator.remove();
            }
        }
    }

}
