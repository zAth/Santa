package me.zath.santa.objects;
/*
 * MC 
 * Created by zAth
 */

import me.zath.santa.Santa;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Boat;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class Sleigh {

    private Player player;
    private Boat boat;
    private Horse horse, horse1, horse2;
    private boolean stopped;

    public Sleigh(Player player) {
        this.player = player;
        stopped = true;

        boat = (Boat) player.getWorld().spawnEntity(player.getLocation(), EntityType.BOAT);
        boat.setWorkOnLand(true);

        Location location = player.getLocation().clone();
        location.setPitch(player.getLocation().getPitch());
        location.setYaw(player.getLocation().getYaw());
        location.add(Santa.getSanta().getUtils().getVector(player.getLocation().getYaw()));
        horse = (Horse) player.getWorld().spawnEntity(location, EntityType.HORSE);
        horse.setAdult();
        horse.setCarryingChest(false);
        horse.setColor(Horse.Color.BROWN);
        horse.setVariant(Horse.Variant.MULE);
        horse.setLeashHolder(player);

        Location location1 = location.clone();
        location1.add(Santa.getSanta().getUtils().getVector(player.getLocation().getYaw()));
        horse1 = (Horse) player.getWorld().spawnEntity(location1, EntityType.HORSE);
        horse1.setAdult();
        horse1.setCarryingChest(false);
        horse1.setColor(Horse.Color.BROWN);
        horse1.setVariant(Horse.Variant.MULE);
        horse1.setLeashHolder(player);

        Location location2 = location1.clone();
        location2.add(Santa.getSanta().getUtils().getVector(player.getLocation().getYaw()));
        horse2 = (Horse) player.getWorld().spawnEntity(location2, EntityType.HORSE);
        horse2.setAdult();
        horse2.setCarryingChest(false);
        horse2.setColor(Horse.Color.BROWN);
        horse2.setVariant(Horse.Variant.MULE);
        horse2.setLeashHolder(player);

        new BukkitRunnable() {
            @Override
            public void run() {
                boat.setPassenger(player);
            }
        }.runTaskLater(Santa.getSanta(), 1);
    }

    public void remove(){
        horse.remove();
        horse1.remove();
        horse2.remove();
        boat.remove();
    }

    /**
     * returns true if the boat doesnt have a passenger of if the passenger isnt the original passenger
     * */
    public boolean tick(){
        if(!stopped) {
            boat.setVelocity(player.getLocation().getDirection().multiply(2));
            boat.getWorld().playSound(boat.getLocation(), Sound.HORSE_GALLOP, 1, 1);
        } else {
            boat.setVelocity(new Vector(0, 0.125, 0));
        }
        horse.setVelocity(new Vector(0, 0.125, 0));
        horse1.setVelocity(new Vector(0, 0.125, 0));
        horse2.setVelocity(new Vector(0, 0.125, 0));

        Location location = player.getLocation().clone();
        location.setPitch(player.getLocation().getPitch());
        location.setYaw(player.getLocation().getYaw());
        location.add(Santa.getSanta().getUtils().getVector(player.getLocation().getYaw()));

        Location location1 = location.clone();
        location1.add(Santa.getSanta().getUtils().getVector(player.getLocation().getYaw()));

        Location location2 = location1.clone();
        location2.add(Santa.getSanta().getUtils().getVector(player.getLocation().getYaw()));

        horse.teleport(location);
        horse1.teleport(location1);
        horse2.teleport(location2);

        boat.teleport(player.getLocation());

        if(boat.getPassenger() == null || !boat.getPassenger().equals(player))
            return true;

        return false;
    }

    public boolean isReindeer(Horse horse){
        return this.horse.equals(horse) || this.horse1.equals(horse) || this.horse2.equals(horse);
    }

    public void drop(){
        Santa.getSanta().getDropManager().drop(player.getItemInHand(), player.getLocation());
    }

    public boolean isStopped(){
        return stopped;
    }

    public void setStopped(boolean stopped){
        this.stopped = stopped;
    }
}
