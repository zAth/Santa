package me.zath.santa.utils;
/*
 * MC 
 * Created by zAth
 */

import org.bukkit.util.Vector;

public class Utils {

    private String getCardinalDirection(float yaw) {
        double rotation = (yaw - 90) % 360;
        if (rotation < 0) {
            rotation += 360.0;
        }
        if (0 <= rotation && rotation < 22.5) {
            return "W";
        } else if (22.5 <= rotation && rotation < 67.5) {
            return "NW";
        } else if (67.5 <= rotation && rotation < 112.5) {
            return "N";
        } else if (112.5 <= rotation && rotation < 157.5) {
            return "NE";
        } else if (157.5 <= rotation && rotation < 202.5) {
            return "E";
        } else if (202.5 <= rotation && rotation < 247.5) {
            return "SE";
        } else if (247.5 <= rotation && rotation < 292.5) {
            return "S";
        } else if (292.5 <= rotation && rotation < 337.5) {
            return "Sw";
        } else if (337.5 <= rotation && rotation < 360.0) {
            return "S";
        } else {
            return null;
        }
    }


    public Vector getVector(float yaw) {
        switch (getCardinalDirection(yaw)) {
            case "N":
                return new Vector(0, 0, -2);
            case "NE":
                return new Vector(2, 0, -2);
            case "NW":
                return new Vector(-2, 0, -2);
            case "E":
                return new Vector(2, 0, 0);
            case "S":
                return new Vector(0, 0, 2);
            case "SE":
                return new Vector(2, 0, 2);
            case "SW":
                return new Vector(-2, 0, 2);
            case "W":
                return new Vector(-2, 0, 0);
            default:
                return new Vector(0,0,0);
        }
    }

}
