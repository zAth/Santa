package me.zath.santa;
/*
 * MC 
 * Created by zAth
 */

import me.zath.santa.commands.Command;
import me.zath.santa.listeners.DropListener;
import me.zath.santa.listeners.SleighListener;
import me.zath.santa.managers.DropManager;
import me.zath.santa.managers.SantaManager;
import me.zath.santa.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Santa extends JavaPlugin {

    private static Santa santa;
    private Config configuration;
    private Utils utils;
    private SantaManager santaManager;
    private DropManager dropManager;

    @Override
    public void onEnable() {
        santa = this;
        File file = new File(getDataFolder(), "config.yml");
        if (!(file.exists())) {
            try {
                saveResource("config.yml", false);
            } catch (Exception ignored) {
            }
        }
        saveDefaultConfig();
        configuration = new Config();
        utils = new Utils();
        santaManager = new SantaManager();
        dropManager = new DropManager();
        registerCommands();
        registerEvents();
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
        Bukkit.getConsoleSender().sendMessage("§6" + getDescription().getName() + " §8v.§6" + getDescription().getVersion() + " §8de§6 "
            + getDescription().getAuthors() + " §2Ativado");
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
    }

    @Override
    public void onDisable() {
        santaManager.getRunnable().cancel();
        dropManager.clear();
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
        Bukkit.getConsoleSender().sendMessage("§6" + getDescription().getName() + " §8v.§6" + getDescription().getVersion() + " §8de§6 "
            + getDescription().getAuthors() + " §4Desativado");
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
        HandlerList.unregisterAll();
    }

    private void registerCommands() {
        getServer().getPluginCommand("santa").setExecutor(new Command());
    }

    private void registerEvents() {
        getServer().getPluginManager().registerEvents(new DropListener(), this);
        getServer().getPluginManager().registerEvents(new SleighListener(), this);
    }

    public static Santa getSanta () {
        return santa;
    }

    public Config getConfiguration() {
        return configuration;
    }

    public Utils getUtils() {
        return utils;
    }

    public SantaManager getSantaManager() {
        return santaManager;
    }

    public DropManager getDropManager() {
        return dropManager;
    }
}
